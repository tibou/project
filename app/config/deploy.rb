set :application, "Project"
set :domain, "188.165.212.121"
set :deploy_to, "/var/www/vhosts/astrolab.tn/sfe.astrolab.tn"
set :app_path, "app"

set :repository, "https://tibou@bitbucket.org/tibou/project.git"
set :scm, :git

set :model_manager, "doctrine"

role :web, domain
role :app, domain, :primary => true

set :use_sudo, false
set :user, "astrolab"

set  :keep_releases, 3

set :dump_assetic_assets, false
set :use_composer, true

set :shared_files, ["app/config/parameters.yml"]
set :shared_children, [app_path + "/logs", web_path + "/uploads", "vendor", app_path + "/sessions"]

set :writable_dirs, ["app/cache", "app/logs", "app/sessions"]
set :webserver_user, "www-data"
set :permission_method, :acl
set :use_set_permissions, true

ssh_options[:forward_agent] = true
default_run_options[:pty] = true
logger.level = Logger::MAX_LEVEL